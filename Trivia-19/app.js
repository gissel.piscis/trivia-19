

function start(){
   
  const name = document.getElementById("name-text").value;
  const str= name.charAt(0).toUpperCase() + name.slice(1)
  

  document.getElementById("empty").innerText = "Genial " + str +"!";  

  if(str == ""){
    alert('Pon tu nombre :D')
    return false;
  }

  document.getElementById("home-box").style.display = "none"; 
  document.getElementById("choice-box").style.display= "block";

}

function fakeNewsStart() 
{
  document.getElementById("first-box").style.display = "block";
  document.getElementById("choice-box").style.display = "none";
}

function infodemiaStart() 
{
  document.getElementById("second-box").style.display = "block";
  document.getElementById("choice-box").style.display = "none";
}


function contadorFakeNews(){

  let contadorGeneral = 0;

  if(document.getElementById("2").checked ){
    contadorGeneral ++;
  }

  if(document.getElementById("6").checked ){
    contadorGeneral ++;
  }

  if(document.getElementById("7").checked ){
    contadorGeneral ++;
  }

  //  alert(contadorGeneral);
  document.getElementById("first-answer").innerText = "Tienes "+contadorGeneral + " respuestas correctas";
  document.getElementById("first-box").style.display = "none"; 
  document.getElementById("first-result").style.display= "block";
}

function contadorNoticias(){

  let contadorGeneral = 0;

  if(document.getElementById("12").checked ){
    contadorGeneral ++;
  }

  if(document.getElementById("15").checked ){
    contadorGeneral ++;
  }

  if(document.getElementById("18").checked ){
    contadorGeneral ++;
  }

  //  alert(contadorGeneral);
  document.getElementById("second-answer").innerText = "Tienes "+contadorGeneral + " respuestas correctas";
  document.getElementById("second-box").style.display = "none"; 
  document.getElementById("second-result").style.display= "block";
}

function infodemiaRestart(){
  infodemiaStart()
  restart()
}

function fakeRestart(){
  fakeNewsStart() 
  restart()
}

function restart(){
  document.getElementById("first-result").style.display= "none";
  document.getElementById("second-result").style.display= "none";
  document.getElementById("2").checked = false;
  document.getElementById("6").checked = false;
  document.getElementById("7").checked = false;
  document.getElementById("12").checked = false;
  document.getElementById("15").checked = false;
  document.getElementById("18").checked = false;
  document.getElementById("1").checked = false;
  document.getElementById("3").checked = false;
  document.getElementById("4").checked = false;
  document.getElementById("5").checked = false;
  document.getElementById("8").checked = false;
  document.getElementById("9").checked = false;
  document.getElementById("10").checked = false;
  document.getElementById("11").checked = false;
  document.getElementById("13").checked = false;
  document.getElementById("14").checked = false;
  document.getElementById("16").checked = false;
  document.getElementById("17").checked = false;
 
}

function home(){
  document.getElementById("home-box").style.display="block";
  document.getElementById("name-text").value="";
  restart()
}